import jc from 'js-cookie'

export default {
    token:()=>{
        return jc.get('token')
    },
    requestToken:()=>{
        return jc.get('requestToken')
    },
    animatedBalance:state=>{
        return state.tweenedBalance.toFixed(0);
    }
}