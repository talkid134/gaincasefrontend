import jc from 'js-cookie'
import axios from 'axios'
import Case from "../models/Case";
import Skin from "../models/Skin";
import User from "../models/User";

export default {
    handleNetError(store) {
        return function (res) {
            store.dispatch('showNote', {text: "Потеряно соединение с сервером", color: "error"});
        }
    },
    getAccountInfo(store) {
        if (store.state.user == null) {
            let loader = new Promise(async (resolve, reject) => {
                    if (store.state.isUserLoading) {
                        resolve(await store.state.userInfoLoader);
                        return;
                    }
                    if (jc.get('token') == undefined) {
                        store.state.isLogged = false;
                        resolve(null);
                    } else if (store.state.isLogged) {
                        store.state.isUserLoading = true;
                        store.state.userInfoLoader = loader;
                        console.log("Получаем информацию о вашем профиле");
                        store.state.axios.post("/user").then(res => {
                            console.log("Данные найдены");
                            var user: User = res.data.data;
                            jc.set('steamid', user.steamId64String);
                            store.commit('setUser', user);
                            store.commit('setBalance', user.balance);
                            store.commit('changeLang', user.language);
                            if (!user.hasTradeLink)
                                store.state.modals.tradelink = true;
                            store.state.isUserLoading = false;
                            store.state.isLogged = true;
                            resolve(user);
                        }).catch(exp => {
                            if (exp.response.status == 401) {
                                store.state.isLogged = false;
                                jc.remove('steamid');
                                jc.remove('requestToken');
                                store.state.isLogged = false;
                            }
                        })
                    }
                    else {
                        resolve(null);
                    }
                }
            );
            store.state.userInfoLoader = loader;
            return loader;
        }
        else {
            return store.state.user;
        }
    },
    getCasesInfo(store) {
        const loader = new Promise(async (resolve, reject) => {
            var url = store.state.config.apiUrl + "/store/cases";
            if (store.state.isCasesLoading) {
                resolve(await store.state.casesLoader);
            } else {
                store.state.isCasesLoading = true;

                if (store.state.cases.length == 0) {
                    axios.post(url, {}).then(res => {
                        store.state.isCasesLoading = false;
                        let json = res.data.data;
                        let cases = [];
                        for (let c of json) {
                            let info: Case = c;
                            info.color = '#' + c.color;
                            info.img_Src = c.img_Src;
                            cases.push(info);
                        }
                        store.commit('setCases', cases);
                        resolve(cases);
                    }).catch(await store.dispatch('handleNetError'));
                } else {
                    store.state.isCasesLoading = false;
                    resolve(store.state.cases)
                }

            }
        });
        store.state.casesLoader = loader;
        return loader;
    }
    ,
    login(store) {
        function S4() {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        }

        const guid = (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
        jc.set("requestToken", guid);
        let refC = jc.get('ref');
        let referal = refC != undefined ? ("masterLink=" + refC + "&") : "";
        let api = store.state.config.apiUrl + "/auth/handleSteam?" + referal;
        jc.remove('ref');
        let url: string = "https://steamcommunity.com/openid/login?" +
            "openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&" +
            "openid.mode=checkid_setup&" +
            "openid.return_to=" + api + "&" +
            "openid.realm=" + api + "&" +
            "openid.ns.ax=http%3A%2F%2Fopenid.net%2Fsrv%2Fax%2F1.0&" +
            "openid.ax.mode=fetch_request&" +
            "openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&" +
            "openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select";
        window.location.assign(url);
    }
    ,
    logout(store) {
        axios.get(store.state.config.apiUrl + "/auth/logout?token=" + jc.get("token"));
        jc.remove('token');
        store.state.isLogged = false;
    }
    ,
    openLoginDialog(store) {
        store.state.isLoginDialogVisible = true;
    }
    ,
    async openSellModal(store, skin) {
        console.log(skin);
        store.state.modals.sellSkin.case = await store.dispatch('getCase', skin.result.caseId);
        store.state.modals.sellSkin.isVisible = true;
        store.state.modals.sellSkin.skin = skin;
    }
    ,
    showNote(store, params) {
        store.state.isNotificationVisible = true;
        store.state.notificationText = params.text;
        store.state.notificationColor = params.color;
    }
    ,
    getUpdates: function (store) {
        let token = store.state.events.sessionToken;
        if (token == null) {
            let S4 = function () {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            };

            token = (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
            store.state.events.sessionToken = token;
        }

        axios.post(store.state.config.apiUrl + "/events", {
            sessionToken: token,
            ts: store.state.events.lastTs
        }).then(async res => {
            var pendingSkins = [];

            let schedule = (count) => {
                store.commit('addLiveDropSkin', pendingSkins.pop());
                if (pendingSkins.length > 0)
                    setTimeout(() => {
                        schedule(count)
                    }, (store.state.updateInterval / (count)) * 1000);
            };

            for (let event of res.data.data.events) {
                switch (event.type) {
                    case "Online": {
                        store.state.stats.online = event.data;
                    }
                        break;
                    case "LiveDrop": {
                        let skin = event.data;
                        store.state.stats.totalOpenedCount++;
                        if (skin.ownerId != store.state.user.userId) {
                            console.log(skin.ownerId);
                            skin.case = await store.dispatch("getCase", skin.caseId);
                            pendingSkins.push(skin);
                        }
                    }
                        break;
                    case "UsersCount": {
                        store.state.stats.usersCount = event.data;
                    }
                        break;
                }
            }
            if (pendingSkins.length !== 0)
                schedule(pendingSkins.length);

            store.state.events.lastTs = res.data.data.ts;
            setTimeout(() => {
                store.dispatch('getUpdates')
            }, store.state.events.updateInterval * 1000);
        });
    }
    ,
    preloadLiveDrop(store, count) {
        if (store.state.liveDropSkins.length == 0) {
            axios.post(store.state.config.apiUrl + "/store/randomResults", {
                count: count,
                offset: 0
            }).then(async res => {
                res.data.data.reverse();
                for (var s of res.data.data) {
                    var skin: Skin = s;
                    skin.case = await store.dispatch("getCase", s.caseId);
                    skin.preload = true;
                    store.state.liveDropSkins.unshift(skin);
                }
            })
        }
    }
    ,
    getCase(store, caseId) {
        return store.state.cases.find(p => p.caseId == caseId);
    }
}