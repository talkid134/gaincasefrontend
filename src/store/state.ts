import Stats from "../models/Stats";

export default {
    user: null,
    userInfoLoader: null,
    isUserLoading: false,
    tweenedBalance: 0,

    stats: {
        totalOpenedCount: -1,
        online: -1,
        usersCount: -1
    },

    events: {
        lastTs: -1,
        updateInterval: 10,
        sessionToken: null
    },

    isLogged: false,

    language: 'ru',
    moment:null,
    config: null,

    liveDropSkins: [],
    liveDropSkinWidth: 100,
    liveDropSkinsCount: 0,

    isNotificationVisible: false,
    notificationText: "",
    notificationColor: "primary",

    isCasesLoading: false,
    cases: [],
    casesLoader: null,

    axios: null,

    modals: {
        topUp: false,
        postPayment: false,
        sellSkin: {
            skin: null,
            isVisible: false,
            isSelling: false,
            case: null,
        },
        tradelink: false
    },
    faq:false,

    VKWidget: null
}