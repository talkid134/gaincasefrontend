import Vuex from 'vuex';
import i18n from 'vue-i18n'
import Vue from 'vue';
import m from 'moment'

export default {
    setBalance(state, val:number){
        state.user.balance = val;
    },
    showTopUpDialog(state){
        state.isTopUpDialogVisible = true;
    },
    addLiveDropSkin(state, skin){
        console.log(skin)
        state.liveDropSkins.unshift(skin);
        state.liveDropSkins.pop();
    },
    setCases(state, cases){
        state.cases = cases;
    },
    setUser(state, user){
        state.user = user;
    }
}