export default interface Case{
    name:string;
    caseId:string;
    color:string;
    price:number;
    img_Src:string;
    maxPrice:number;
    minPrice:number;
    maxNumber:number;
    minNumber:number;
    available:boolean;
    history:Array<any>;
}