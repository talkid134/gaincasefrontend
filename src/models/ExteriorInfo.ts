export default class ExteriorInfo{
    classId:number;
    assetId:number;
    count:number;
    botSteamId:string;
    exterior:string;
    price:number;
    number:number;
    isLocked:boolean;
    unlockTime:Date;
}