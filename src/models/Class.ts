
export default interface Class{
    icon_Url:string;
    iconUrl:string;
    weapon:string;
    name:string;
    statTrak:boolean;
    exterior:string;
    name_Color:string;
    fromNumber:number;
    toNumber:number;
    number:number;
}