export default class RandomResult{
    signature:string;
    random:any;
    completionTime:string;
    hashedApiKey:string;
    number:number;
    caseId:string;
    serialNumber:number;
}