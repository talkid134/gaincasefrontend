export default interface User{
    name:string;
    steamId64:string;
    steamId32:number;
    steamId64String:string;
    registrationDate:Date;
    balance:number;
    totalBought:number;
    hasTradeLink:boolean;
    iconUrl:string;
    language:string;
    referalLink:string;
}