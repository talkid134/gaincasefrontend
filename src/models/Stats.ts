
export default interface Stats{
    online:number;
    totalOpenedCount:number;
    usersCount:number;
}