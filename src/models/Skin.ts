import Case from "./Case";
import Class from "./Class";
import RandomResult from "./RandomResult";

export default interface Skin{
    step:number;
    time:number;
    style:string;

    price:number;
    unlockTime:Date;

    casePrice:number;

    assetId:number;
    classId:number;
    caseId:string;

    class:Class;
    case:Case;
    result:RandomResult;

    isTarget:boolean;

    canWatch:boolean;
    isSlim:boolean;
    isLocked:boolean;
    isDeparted:boolean;
    isOwner:boolean;
    isClass:boolean;
    isSold:boolean;
    canSell:boolean;

    onSell:Function;

    id:number;
    botSteamId64:string;
    ownerSteamId64:number;
    ownerSteamId64String:string;
    ownername:string;

    isMouseOver:boolean;
}