import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/HomeView'
import OpenCase from './views/OpenCaseView'
import InventoryView from './views/InventoryView'
import SkinsTableView from './views/SkinsTableView'
import ReferalView from './views/ReferalView'
import AdminPanel from './views/AdminPanel'
import FAQView from './views/FAQ'
import AgreementView from './views/AgreementView'
Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/opencase/',
            name: 'opencase',
            props:true,
            component: OpenCase
        },
        {
            path: '/skinstable/',
            name: 'skinstable',
            props:true,
            component: SkinsTableView
        },
        {
            path: '/inventory/',
            name: 'inventory',
            component: InventoryView
        },
        {
            path: '/referal/',
            name: 'referal',
            component: ReferalView
        },
        {
            path: '/a57b184e2968cd/',
            name: 'adminpanel',
            component: AdminPanel
        },{
            path: '/faq',
            name: 'faq',
            component: FAQView
        },
        {
            path: '/agreement',
            name: 'agreement',
            component: AgreementView
        }
    ]
})
