import Vue, {DirectiveOptions} from 'vue'
import {copyToClipboard} from '../Helpers'

Vue.directive('click-to-copy',function (el:HTMLElement, bind, node) {
    let handler;

    function copy(val){
        copyToClipboard(val);
        node.context.$store.dispatch('showNote', {
            text:`Текст <${val}> был скопирован в буфер обмена`,
            color:"info"
        })
    }

    if(bind.modifiers['text']!==undefined){
        handler = (e:MouseEvent)=>{
            copy(el.innerText)
        }
    }else{
        handler = (e:MouseEvent) => {
            copy(bind.value(el, node))
        }
    }
    el.onmousedown = handler;
});