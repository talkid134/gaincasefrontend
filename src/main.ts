import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import i18n from './i18n'
import '@mdi/font/css/materialdesignicons.min.css'

import config from './config/config.json'
import axios from 'axios'

import  './directives/clicktocopy'

import jc from 'js-cookie'

Vue.config.productionTip = false;

document.querySelector("html").addEventListener("contextmenu", function (ev) {
    //ev.preventDefault()
});

if(jc.get("dev")!=undefined){
    config.apiUrl = "http://localhost:53137";
    config.staticUrl = "http://localhost:8080";
}

store.state.config = config;

store.state.axios = axios.create({
    baseURL:config.apiUrl
});

new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app');
